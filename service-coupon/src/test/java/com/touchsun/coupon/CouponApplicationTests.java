package com.touchsun.coupon;

import com.touchsun.coupon.service.CouponService;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
class CouponApplicationTests {

    @Autowired
    CouponService couponService;

    @Test
    void add() {
        System.out.println(couponService.list());
    }

}
