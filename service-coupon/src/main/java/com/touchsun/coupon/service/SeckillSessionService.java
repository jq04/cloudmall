package com.touchsun.coupon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.touchsun.common.utils.PageUtils;
import com.touchsun.coupon.entity.SeckillSessionEntity;

import java.util.Map;

/**
 * 秒杀活动场次
 *
 * @author touchsun
 * @email mr.jiaqi1204@gmail.com
 * @date 2023-02-15 10:48:57
 */
public interface SeckillSessionService extends IService<SeckillSessionEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

