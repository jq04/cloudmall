package com.touchsun.coupon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.touchsun.common.utils.PageUtils;
import com.touchsun.coupon.entity.MemberPriceEntity;

import java.util.Map;

/**
 * 商品会员价格
 *
 * @author touchsun
 * @email mr.jiaqi1204@gmail.com
 * @date 2023-02-15 10:48:57
 */
public interface MemberPriceService extends IService<MemberPriceEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

