package com.touchsun.coupon.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.touchsun.common.utils.PageUtils;
import com.touchsun.common.utils.Query;
import com.touchsun.coupon.dao.MemberPriceDao;
import com.touchsun.coupon.entity.MemberPriceEntity;
import com.touchsun.coupon.service.MemberPriceService;
import org.springframework.stereotype.Service;

import java.util.Map;


@Service("memberPriceService")
public class MemberPriceServiceImpl extends ServiceImpl<MemberPriceDao, MemberPriceEntity> implements MemberPriceService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<MemberPriceEntity> page = this.page(
                new Query<MemberPriceEntity>().getPage(params),
                new QueryWrapper<MemberPriceEntity>()
        );

        return new PageUtils(page);
    }

}