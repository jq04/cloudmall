package com.touchsun.coupon.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.touchsun.coupon.entity.SpuBoundsEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品spu积分设置
 *
 * @author touchsun
 * @email mr.jiaqi1204@gmail.com
 * @date 2023-02-15 10:48:57
 */
@Mapper
public interface SpuBoundsDao extends BaseMapper<SpuBoundsEntity> {

}
