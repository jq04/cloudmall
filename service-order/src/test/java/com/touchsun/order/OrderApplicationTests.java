package com.touchsun.order;

import com.touchsun.order.service.OrderService;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
class OrderApplicationTests {

    @Autowired
    OrderService orderService;

    @Test
    void contextLoads() {
        System.out.println(orderService.list());
    }

}
