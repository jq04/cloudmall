package com.touchsun.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.touchsun.common.utils.PageUtils;
import com.touchsun.order.entity.OrderSettingEntity;

import java.util.Map;

/**
 * 订单配置信息
 *
 * @author touchsun
 * @email mr.jiaqi1204@gmail.com
 * @date 2023-02-15 11:00:27
 */
public interface OrderSettingService extends IService<OrderSettingEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

