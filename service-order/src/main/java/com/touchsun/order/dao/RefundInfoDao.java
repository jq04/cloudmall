package com.touchsun.order.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.touchsun.order.entity.RefundInfoEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 退款信息
 *
 * @author touchsun
 * @email mr.jiaqi1204@gmail.com
 * @date 2023-02-15 11:00:27
 */
@Mapper
public interface RefundInfoDao extends BaseMapper<RefundInfoEntity> {

}
