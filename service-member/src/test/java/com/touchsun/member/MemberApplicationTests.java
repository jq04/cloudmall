package com.touchsun.member;

import com.touchsun.member.service.MemberService;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
class MemberApplicationTests {

    @Autowired
    MemberService memberService;

    @Test
    void list() {
        System.out.println(memberService.list());
    }

}
