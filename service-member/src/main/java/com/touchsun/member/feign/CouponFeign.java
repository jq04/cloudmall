package com.touchsun.member.feign;

import com.touchsun.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 优惠券远程调用
 *
 * @author lee
 * @since 2023/2/16 14:57
 */
@FeignClient("service-coupon")
public interface CouponFeign {

    @RequestMapping("coupon/coupon/list/all")
    R listAll();
}
