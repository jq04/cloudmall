package com.touchsun.member.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.touchsun.member.entity.MemberLevelEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 会员等级
 *
 * @author touchsun
 * @email mr.jiaqi1204@gmail.com
 * @date 2023-02-15 10:55:53
 */
@Mapper
public interface MemberLevelDao extends BaseMapper<MemberLevelEntity> {

}
