# cloudmall

# 序章

> 微服务电商框架，既谷粒商城学习记录

> - 基本的程序搭建不做记录，从数据库的SQL开始
> - 基本的中间件安装不做记录, 默认都已具备条件
> - 本篇文档仅仅记录关键知识点及信息

> 项目Spring组件的版本选择
>
> | Spring Cloud Alibaba Version | Spring Cloud Version  | Spring Boot Version |
> | ---------------------------- | --------------------- | ------------------- |
> | 2021.0.1.0                   | Spring Cloud 2021.0.1 | 2.6.3               |

## 1. 初始化

### 1.1 数据库初始化脚本

    详见Doc文件夹下的SQL脚本

### 1.2 管理系统脚手架

> 人人开源
>
> - 后端工程：https://gitee.com/renrenio/renren-fast.git
> - 前端工程：https://gitee.com/renrenio/renren-fast-vue.git

#### 1.2.1 集成

1. 拉取代码

2. 删除不必要的文件

3. 修改pom文件使其与本项目父子联动

4. 新建数据库`service_admin`

5. 执行数据库初始化SQL

6. 修改程序数据库配置

7. 修改前端程序baseUrl

   ![image-20230214145525404](https://gitee.com/jq04/lee-image-host/raw/master/img/image-20230214145525404.png)

8. 账号/密码 admin/admin

### 1.3 代码生成器

> 人人开源
>
> - 后端工程：https://gitee.com/renrenio/renren-generator.git

#### 1.3.1 集成

1. 拉取代码

2. 删除不必要的文件

3. 修改pom文件使其与本项目父子联动

4. 修改yml指定针对哪个数据库进行代码生成

   举例 商品系统的生成

   ![image-20230214150437006](https://gitee.com/jq04/lee-image-host/raw/master/img/image-20230214150437006.png)

5. 配置分页器循环依赖问题

   spring.main.allow-circular-references = true

   ![image-20230214152524887](https://gitee.com/jq04/lee-image-host/raw/master/img/image-20230214152524887.png)

6. 启动程序选择表即可生成CRUD代码

### 1.4 抽取公共模块

> 由于许多模块需要用到相同的功能，所有抽取common工程，作为各个模块的引用

## 2. Docker

### 2.1 nacos

```shell
docker run --name nacos -d -p 8848:8848 -p 9848:9848 -p 9849:9849 --privileged=true -e MODE=standalone 440657d52bc3
```

# 坑-Nacos

## 1. BootStrap文件报错

    由于SpringCloud在2020版本后抛弃了BootStrap文件, 如果使用则需要单独引入
```xml
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-bootstrap</artifactId>
</dependency>
```

## 2. 配置不刷新

增加动态刷新配置注解`@RefreshScope`

## 3. 知识点

### 3.1 优先级

配置中心 > 本地文件

### 3.2 命名空间概念

1. 概念：为了数据隔离
2. 默认：默认的命名空间走的是public

使用场景

1. 使用命名空间做环境隔离 **在bootstrap里指定空间ID即可**
2. 微服务之间的配置隔离 **为每个微服务新建一个空间，后期每个微服务都读取各自空间的配置**

### 3.3 配置集

1. 多有的配置的并集称为配置集
2. **配置集ID**: 就是界面上新建配置时候的 **DataId**

### 3.4 配置分组

## 微服务网关

### 1. 意义

- 鉴权
- 日志
- 路由

### 2. 关键术语

- 断言
- 过滤
- 路由

> **流程大概**
>
> 请求 -> |GateWay| -> 经过断言判断去哪个路由 -> 经过一些列的Filter -> 落到真实的服务 -> 经过一些列的Filter -> 结束

### 3. 配置详解

#### 3.1 路由

**地址栏查询参数路由匹配**

```yaml
spring:
  cloud:
    gateway:
      routes: 
        - id: link_baidu # 必须指定的一个ID
          uri: https://www.baidu.com/ # 路由的目标地址
          predicates:
            - Query=url, baidu # 含有参数[url], 并且参数满足正则表达式[$baidu^]

        - id: link_qq # 必须指定的一个ID
          uri: https://www.qq.com/ # 路由的目标地址
          predicates:
            - Query=url, qq # 含有参数[url], 并且参数满足正则表达式[$qq^]
```

#### 3.4 跨域配置问题

在使用SpringBoot2.6.x时跨域配置应注意此处

```java
package com.touchsun.gateway.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.reactive.CorsWebFilter;
import org.springframework.web.cors.reactive.UrlBasedCorsConfigurationSource;

/**
 * 统一跨域请求配置
 *
 * @author lee
 * @since 2023/2/26 22:15
 */
@Configuration
public class CorsConfig {

    /**
     * 跨域方法配置
     * 
     * @return 跨域配置对象
     */
    @Bean
    public CorsWebFilter corsWebFilter() {
        // 配置对象[使用URL配置]
        UrlBasedCorsConfigurationSource configSource = new UrlBasedCorsConfigurationSource();
        // 跨域配置 *表示所有
        CorsConfiguration config = new CorsConfiguration();
        
        // - 允许跨域的HTTP请求方法类型
        config.addAllowedMethod("*");
        // - 允许跨域的请求头
        config.addAllowedHeader("*");
        // - 允许跨域的源的头
        config.addAllowedOriginPattern("*"); // 新版本的写法
        // - 允许携带Cookie跨域
        config.setAllowCredentials(true);
        
        // 配置路径规则[/**:所有路径]
        configSource.registerCorsConfiguration("/**", config);
        return new CorsWebFilter(configSource);
    }
}

```























