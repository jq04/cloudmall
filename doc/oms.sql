drop table if exists oms_order;

drop table if exists oms_order_item;

drop table if exists oms_order_operate_history;

drop table if exists oms_order_return_apply;

drop table if exists oms_order_return_reason;

drop table if exists oms_order_setting;

drop table if exists oms_payment_info;

drop table if exists oms_refund_info;

/*==============================================================*/
/* Table: oms_order                                             */
/*==============================================================*/
create table oms_order
(
    id                      bigint not null auto_increment comment 'id',
    member_id               bigint comment 'member_id',
    order_sn                char(32) comment 'è®¢åå·',
    coupon_id               bigint comment 'ä½¿ç¨çä¼æ å¸',
    create_time             datetime comment 'create_time',
    member_username         varchar(200) comment 'ç¨æ·å',
    total_amount            decimal(18, 4) comment 'è®¢åæ»é¢',
    pay_amount              decimal(18, 4) comment 'åºä»æ»é¢',
    freight_amount          decimal(18, 4) comment 'è¿è´¹éé¢',
    promotion_amount        decimal(18, 4) comment 'ä¿éä¼åéé¢ï¼ä¿éä»·ãæ»¡åãé¶æ¢¯ä»·ï¼',
    integration_amount      decimal(18, 4) comment 'ç§¯åæµæ£éé¢',
    coupon_amount           decimal(18, 4) comment 'ä¼æ å¸æµæ£éé¢',
    discount_amount         decimal(18, 4) comment 'åå°è°æ´è®¢åä½¿ç¨çææ£éé¢',
    pay_type                tinyint comment 'æ¯ä»æ¹å¼ã1->æ¯ä»å®ï¼2->å¾®ä¿¡ï¼3->é¶èï¼ 4->è´§å°ä»æ¬¾ï¼ã',
    source_type             tinyint comment 'è®¢åæ¥æº[0->PCè®¢åï¼1->appè®¢å]',
    status                  tinyint comment 'è®¢åç¶æã0->å¾ä»æ¬¾ï¼1->å¾åè´§ï¼2->å·²åè´§ï¼3->å·²å®æï¼4->å·²å³é­ï¼5->æ æè®¢åã',
    delivery_company        varchar(64) comment 'ç©æµå¬å¸(ééæ¹å¼)',
    delivery_sn             varchar(64) comment 'ç©æµåå·',
    auto_confirm_day        int comment 'èªå¨ç¡®è®¤æ¶é´ï¼å¤©ï¼',
    integration             int comment 'å¯ä»¥è·å¾çç§¯å',
    growth                  int comment 'å¯ä»¥è·å¾çæé¿å¼',
    bill_type               tinyint comment 'åç¥¨ç±»å[0->ä¸å¼åç¥¨ï¼1->çµå­åç¥¨ï¼2->çº¸è´¨åç¥¨]',
    bill_header             varchar(255) comment 'åç¥¨æ¬å¤´',
    bill_content            varchar(255) comment 'åç¥¨åå®¹',
    bill_receiver_phone     varchar(32) comment 'æ¶ç¥¨äººçµè¯',
    bill_receiver_email     varchar(64) comment 'æ¶ç¥¨äººé®ç®±',
    receiver_name           varchar(100) comment 'æ¶è´§äººå§å',
    receiver_phone          varchar(32) comment 'æ¶è´§äººçµè¯',
    receiver_post_code      varchar(32) comment 'æ¶è´§äººé®ç¼',
    receiver_province       varchar(32) comment 'çä»½/ç´è¾å¸',
    receiver_city           varchar(32) comment 'åå¸',
    receiver_region         varchar(32) comment 'åº',
    receiver_detail_address varchar(200) comment 'è¯¦ç»å°å',
    note                    varchar(500) comment 'è®¢åå¤æ³¨',
    confirm_status          tinyint comment 'ç¡®è®¤æ¶è´§ç¶æ[0->æªç¡®è®¤ï¼1->å·²ç¡®è®¤]',
    delete_status           tinyint comment 'å é¤ç¶æã0->æªå é¤ï¼1->å·²å é¤ã',
    use_integration         int comment 'ä¸åæ¶ä½¿ç¨çç§¯å',
    payment_time            datetime comment 'æ¯ä»æ¶é´',
    delivery_time           datetime comment 'åè´§æ¶é´',
    receive_time            datetime comment 'ç¡®è®¤æ¶è´§æ¶é´',
    comment_time            datetime comment 'è¯ä»·æ¶é´',
    modify_time             datetime comment 'ä¿®æ¹æ¶é´',
    primary key (id)
);

alter table oms_order
    comment 'è®¢å';

/*==============================================================*/
/* Table: oms_order_item                                        */
/*==============================================================*/
create table oms_order_item
(
    id                 bigint not null auto_increment comment 'id',
    order_id           bigint comment 'order_id',
    order_sn           char(32) comment 'order_sn',
    spu_id             bigint comment 'spu_id',
    spu_name           varchar(255) comment 'spu_name',
    spu_pic            varchar(500) comment 'spu_pic',
    spu_brand          varchar(200) comment 'åç',
    category_id        bigint comment 'åååç±»id',
    sku_id             bigint comment 'ååskuç¼å·',
    sku_name           varchar(255) comment 'ååskuåå­',
    sku_pic            varchar(500) comment 'ååskuå¾ç',
    sku_price          decimal(18, 4) comment 'ååskuä»·æ ¼',
    sku_quantity       int comment 'ååè´­ä¹°çæ°é',
    sku_attrs_vals     varchar(500) comment 'ååéå®å±æ§ç»åï¼JSONï¼',
    promotion_amount   decimal(18, 4) comment 'ååä¿éåè§£éé¢',
    coupon_amount      decimal(18, 4) comment 'ä¼æ å¸ä¼æ åè§£éé¢',
    integration_amount decimal(18, 4) comment 'ç§¯åä¼æ åè§£éé¢',
    real_amount        decimal(18, 4) comment 'è¯¥ååç»è¿ä¼æ åçåè§£éé¢',
    gift_integration   int comment 'èµ éç§¯å',
    gift_growth        int comment 'èµ éæé¿å¼',
    primary key (id)
);

alter table oms_order_item
    comment 'è®¢åé¡¹ä¿¡æ¯';

/*==============================================================*/
/* Table: oms_order_operate_history                             */
/*==============================================================*/
create table oms_order_operate_history
(
    id           bigint not null auto_increment comment 'id',
    order_id     bigint comment 'è®¢åid',
    operate_man  varchar(100) comment 'æä½äºº[ç¨æ·ï¼ç³»ç»ï¼åå°ç®¡çå]',
    create_time  datetime comment 'æä½æ¶é´',
    order_status tinyint comment 'è®¢åç¶æã0->å¾ä»æ¬¾ï¼1->å¾åè´§ï¼2->å·²åè´§ï¼3->å·²å®æï¼4->å·²å³é­ï¼5->æ æè®¢åã',
    note         varchar(500) comment 'å¤æ³¨',
    primary key (id)
);

alter table oms_order_operate_history
    comment 'è®¢åæä½åå²è®°å½';

/*==============================================================*/
/* Table: oms_order_return_apply                                */
/*==============================================================*/
create table oms_order_return_apply
(
    id              bigint not null auto_increment comment 'id',
    order_id        bigint comment 'order_id',
    sku_id          bigint comment 'éè´§ååid',
    order_sn        char(32) comment 'è®¢åç¼å·',
    create_time     datetime comment 'ç³è¯·æ¶é´',
    member_username varchar(64) comment 'ä¼åç¨æ·å',
    return_amount   decimal(18, 4) comment 'éæ¬¾éé¢',
    return_name     varchar(100) comment 'éè´§äººå§å',
    return_phone    varchar(20) comment 'éè´§äººçµè¯',
    status          tinyint(1) comment 'ç³è¯·ç¶æ[0->å¾å¤çï¼1->éè´§ä¸­ï¼2->å·²å®æï¼3->å·²æç»]',
    handle_time     datetime comment 'å¤çæ¶é´',
    sku_img         varchar(500) comment 'ååå¾ç',
    sku_name        varchar(200) comment 'åååç§°',
    sku_brand       varchar(200) comment 'åååç',
    sku_attrs_vals  varchar(500) comment 'ååéå®å±æ§(JSON)',
    sku_count       int comment 'éè´§æ°é',
    sku_price       decimal(18, 4) comment 'åååä»·',
    sku_real_price  decimal(18, 4) comment 'ååå®éæ¯ä»åä»·',
    reason          varchar(200) comment 'åå ',
    descriptionè¿°   varchar(500) comment 'æè¿°',
    desc_pics       varchar(2000) comment 'å­è¯å¾çï¼ä»¥éå·éå¼',
    handle_note     varchar(500) comment 'å¤çå¤æ³¨',
    handle_man      varchar(200) comment 'å¤çäººå',
    receive_man     varchar(100) comment 'æ¶è´§äºº',
    receive_time    datetime comment 'æ¶è´§æ¶é´',
    receive_note    varchar(500) comment 'æ¶è´§å¤æ³¨',
    receive_phone   varchar(20) comment 'æ¶è´§çµè¯',
    company_address varchar(500) comment 'å¬å¸æ¶è´§å°å',
    primary key (id)
);

alter table oms_order_return_apply
    comment 'è®¢åéè´§ç³è¯·';

/*==============================================================*/
/* Table: oms_order_return_reason                               */
/*==============================================================*/
create table oms_order_return_reason
(
    id          bigint not null auto_increment comment 'id',
    name        varchar(200) comment 'éè´§åå å',
    sort        int comment 'æåº',
    status      tinyint(1) comment 'å¯ç¨ç¶æ',
    create_time datetime comment 'create_time',
    primary key (id)
);

alter table oms_order_return_reason
    comment 'éè´§åå ';

/*==============================================================*/
/* Table: oms_order_setting                                     */
/*==============================================================*/
create table oms_order_setting
(
    id                    bigint not null auto_increment comment 'id',
    flash_order_overtime  int comment 'ç§æè®¢åè¶æ¶å³é­æ¶é´(å)',
    normal_order_overtime int comment 'æ­£å¸¸è®¢åè¶æ¶æ¶é´(å)',
    confirm_overtime      int comment 'åè´§åèªå¨ç¡®è®¤æ¶è´§æ¶é´ï¼å¤©ï¼',
    finish_overtime       int comment 'èªå¨å®æäº¤ææ¶é´ï¼ä¸è½ç³è¯·éè´§ï¼å¤©ï¼',
    comment_overtime      int comment 'è®¢åå®æåèªå¨å¥½è¯æ¶é´ï¼å¤©ï¼',
    member_level          tinyint(2) comment 'ä¼åç­çº§ã0-ä¸éä¼åç­çº§ï¼å¨é¨éç¨ï¼å¶ä»-å¯¹åºçå¶ä»ä¼åç­çº§ã',
    primary key (id)
);

alter table oms_order_setting
    comment 'è®¢åéç½®ä¿¡æ¯';

/*==============================================================*/
/* Table: oms_payment_info                                      */
/*==============================================================*/
create table oms_payment_info
(
    id               bigint not null auto_increment comment 'id',
    order_sn         char(32) comment 'è®¢åå·ï¼å¯¹å¤ä¸å¡å·ï¼',
    order_id         bigint comment 'è®¢åid',
    alipay_trade_no  varchar(50) comment 'æ¯ä»å®äº¤ææµæ°´å·',
    total_amount     decimal(18, 4) comment 'æ¯ä»æ»éé¢',
    subject          varchar(200) comment 'äº¤æåå®¹',
    payment_status   varchar(20) comment 'æ¯ä»ç¶æ',
    create_time      datetime comment 'åå»ºæ¶é´',
    confirm_time     datetime comment 'ç¡®è®¤æ¶é´',
    callback_content varchar(4000) comment 'åè°åå®¹',
    callback_time    datetime comment 'åè°æ¶é´',
    primary key (id)
);

alter table oms_payment_info
    comment 'æ¯ä»ä¿¡æ¯è¡¨';

/*==============================================================*/
/* Table: oms_refund_info                                       */
/*==============================================================*/
create table oms_refund_info
(
    id              bigint not null auto_increment comment 'id',
    order_return_id bigint comment 'éæ¬¾çè®¢å',
    refund          decimal(18, 4) comment 'éæ¬¾éé¢',
    refund_sn       varchar(64) comment 'éæ¬¾äº¤ææµæ°´å·',
    refund_status   tinyint(1) comment 'éæ¬¾ç¶æ',
    refund_channel  tinyint comment 'éæ¬¾æ¸ é[1-æ¯ä»å®ï¼2-å¾®ä¿¡ï¼3-é¶èï¼4-æ±æ¬¾]',
    refund_content  varchar(5000),
    primary key (id)
);

alter table oms_refund_info
    comment 'éæ¬¾ä¿¡æ¯';