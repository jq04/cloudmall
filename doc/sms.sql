drop table if exists sms_coupon;

drop table if exists sms_coupon_history;

drop table if exists sms_coupon_spu_category_relation;

drop table if exists sms_coupon_spu_relation;

drop table if exists sms_home_adv;

drop table if exists sms_home_subject;

drop table if exists sms_home_subject_spu;

drop table if exists sms_member_price;

drop table if exists sms_seckill_promotion;

drop table if exists sms_seckill_session;

drop table if exists sms_seckill_sku_notice;

drop table if exists sms_seckill_sku_relation;

drop table if exists sms_sku_full_reduction;

drop table if exists sms_sku_ladder;

drop table if exists sms_spu_bounds;

/*==============================================================*/
/* Table: sms_coupon                                            */
/*==============================================================*/
create table sms_coupon
(
    id                bigint not null auto_increment comment 'id',
    coupon_type       tinyint(1) comment 'ä¼æ å·ç±»å[0->å¨åºèµ å¸ï¼1->ä¼åèµ å¸ï¼2->è´­ç©èµ å¸ï¼3->æ³¨åèµ å¸]',
    coupon_img        varchar(2000) comment 'ä¼æ å¸å¾ç',
    coupon_name       varchar(100) comment 'ä¼æ å·åå­',
    num               int comment 'æ°é',
    amount            decimal(18, 4) comment 'éé¢',
    per_limit         int comment 'æ¯äººéé¢å¼ æ°',
    min_point         decimal(18, 4) comment 'ä½¿ç¨é¨æ§',
    start_time        datetime comment 'å¼å§æ¶é´',
    end_time          datetime comment 'ç»ææ¶é´',
    use_type          tinyint(1) comment 'ä½¿ç¨ç±»å[0->å¨åºéç¨ï¼1->æå®åç±»ï¼2->æå®åå]',
    note              varchar(200) comment 'å¤æ³¨',
    publish_count     int(11) comment 'åè¡æ°é',
    use_count         int(11) comment 'å·²ä½¿ç¨æ°é',
    receive_count     int(11) comment 'é¢åæ°é',
    enable_start_time datetime comment 'å¯ä»¥é¢åçå¼å§æ¥æ',
    enable_end_time   datetime comment 'å¯ä»¥é¢åçç»ææ¥æ',
    code              varchar(64) comment 'ä¼æ ç ',
    member_level      tinyint(1) comment 'å¯ä»¥é¢åçä¼åç­çº§[0->ä¸éç­çº§ï¼å¶ä»-å¯¹åºç­çº§]',
    publish           tinyint(1) comment 'åå¸ç¶æ[0-æªåå¸ï¼1-å·²åå¸]',
    primary key (id)
);

alter table sms_coupon
    comment 'ä¼æ å¸ä¿¡æ¯';

/*==============================================================*/
/* Table: sms_coupon_history                                    */
/*==============================================================*/
create table sms_coupon_history
(
    id               bigint not null auto_increment comment 'id',
    coupon_id        bigint comment 'ä¼æ å¸id',
    member_id        bigint comment 'ä¼åid',
    member_nick_name varchar(64) comment 'ä¼ååå­',
    get_type         tinyint(1) comment 'è·åæ¹å¼[0->åå°èµ éï¼1->ä¸»å¨é¢å]',
    create_time      datetime comment 'åå»ºæ¶é´',
    use_type         tinyint(1) comment 'ä½¿ç¨ç¶æ[0->æªä½¿ç¨ï¼1->å·²ä½¿ç¨ï¼2->å·²è¿æ]',
    use_time         datetime comment 'ä½¿ç¨æ¶é´',
    order_id         bigint comment 'è®¢åid',
    order_sn         bigint comment 'è®¢åå·',
    primary key (id)
);

alter table sms_coupon_history
    comment 'ä¼æ å¸é¢ååå²è®°å½';

/*==============================================================*/
/* Table: sms_coupon_spu_category_relation                      */
/*==============================================================*/
create table sms_coupon_spu_category_relation
(
    id            bigint not null auto_increment comment 'id',
    coupon_id     bigint comment 'ä¼æ å¸id',
    category_id   bigint comment 'äº§ååç±»id',
    category_name varchar(64) comment 'äº§ååç±»åç§°',
    primary key (id)
);

alter table sms_coupon_spu_category_relation
    comment 'ä¼æ å¸åç±»å³è';

/*==============================================================*/
/* Table: sms_coupon_spu_relation                               */
/*==============================================================*/
create table sms_coupon_spu_relation
(
    id        bigint not null auto_increment comment 'id',
    coupon_id bigint comment 'ä¼æ å¸id',
    spu_id    bigint comment 'spu_id',
    spu_name  varchar(255) comment 'spu_name',
    primary key (id)
);

alter table sms_coupon_spu_relation
    comment 'ä¼æ å¸ä¸äº§åå³è';

/*==============================================================*/
/* Table: sms_home_adv                                          */
/*==============================================================*/
create table sms_home_adv
(
    id           bigint not null auto_increment comment 'id',
    name         varchar(100) comment 'åå­',
    pic          varchar(500) comment 'å¾çå°å',
    start_time   datetime comment 'å¼å§æ¶é´',
    end_time     datetime comment 'ç»ææ¶é´',
    status       tinyint(1) comment 'ç¶æ',
    click_count  int comment 'ç¹å»æ°',
    url          varchar(500) comment 'å¹¿åè¯¦æè¿æ¥å°å',
    note         varchar(500) comment 'å¤æ³¨',
    sort         int comment 'æåº',
    publisher_id bigint comment 'åå¸è',
    auth_id      bigint comment 'å®¡æ ¸è',
    primary key (id)
);

alter table sms_home_adv
    comment 'é¦é¡µè½®æ­å¹¿å';

/*==============================================================*/
/* Table: sms_home_subject                                      */
/*==============================================================*/
create table sms_home_subject
(
    id        bigint not null auto_increment comment 'id',
    name      varchar(200) comment 'ä¸é¢åå­',
    title     varchar(255) comment 'ä¸é¢æ é¢',
    sub_title varchar(255) comment 'ä¸é¢å¯æ é¢',
    status    tinyint(1) comment 'æ¾ç¤ºç¶æ',
    url       varchar(500) comment 'è¯¦æè¿æ¥',
    sort      int comment 'æåº',
    img       varchar(500) comment 'ä¸é¢å¾çå°å',
    primary key (id)
);

alter table sms_home_subject
    comment 'é¦é¡µä¸é¢è¡¨ãjdé¦é¡µä¸é¢å¾å¤ä¸é¢ï¼æ¯ä¸ªä¸é¢é¾æ¥æ°çé¡µé¢ï¼å±ç¤ºä¸é¢ååä¿¡æ¯ã';

/*==============================================================*/
/* Table: sms_home_subject_spu                                  */
/*==============================================================*/
create table sms_home_subject_spu
(
    id         bigint not null auto_increment comment 'id',
    name       varchar(200) comment 'ä¸é¢åå­',
    subject_id bigint comment 'ä¸é¢id',
    spu_id     bigint comment 'spu_id',
    sort       int comment 'æåº',
    primary key (id)
);

alter table sms_home_subject_spu
    comment 'ä¸é¢åå';

/*==============================================================*/
/* Table: sms_member_price                                      */
/*==============================================================*/
create table sms_member_price
(
    id                bigint not null auto_increment comment 'id',
    sku_id            bigint comment 'sku_id',
    member_level_id   bigint comment 'ä¼åç­çº§id',
    member_level_name varchar(100) comment 'ä¼åç­çº§å',
    member_price      decimal(18, 4) comment 'ä¼åå¯¹åºä»·æ ¼',
    add_other         tinyint(1) comment 'å¯å¦å å å¶ä»ä¼æ [0-ä¸å¯å å ä¼æ ï¼1-å¯å å ]',
    primary key (id)
);

alter table sms_member_price
    comment 'ååä¼åä»·æ ¼';

/*==============================================================*/
/* Table: sms_seckill_promotion                                 */
/*==============================================================*/
create table sms_seckill_promotion
(
    id          bigint not null auto_increment comment 'id',
    title       varchar(255) comment 'æ´»å¨æ é¢',
    start_time  datetime comment 'å¼å§æ¥æ',
    end_time    datetime comment 'ç»ææ¥æ',
    status      tinyint comment 'ä¸ä¸çº¿ç¶æ',
    create_time datetime comment 'åå»ºæ¶é´',
    user_id     bigint comment 'åå»ºäºº',
    primary key (id)
);

alter table sms_seckill_promotion
    comment 'ç§ææ´»å¨';

/*==============================================================*/
/* Table: sms_seckill_session                                   */
/*==============================================================*/
create table sms_seckill_session
(
    id          bigint not null auto_increment comment 'id',
    name        varchar(200) comment 'åºæ¬¡åç§°',
    start_time  datetime comment 'æ¯æ¥å¼å§æ¶é´',
    end_time    datetime comment 'æ¯æ¥ç»ææ¶é´',
    status      tinyint(1) comment 'å¯ç¨ç¶æ',
    create_time datetime comment 'åå»ºæ¶é´',
    primary key (id)
);

alter table sms_seckill_session
    comment 'ç§ææ´»å¨åºæ¬¡';

/*==============================================================*/
/* Table: sms_seckill_sku_notice                                */
/*==============================================================*/
create table sms_seckill_sku_notice
(
    id            bigint not null auto_increment comment 'id',
    member_id     bigint comment 'member_id',
    sku_id        bigint comment 'sku_id',
    session_id    bigint comment 'æ´»å¨åºæ¬¡id',
    subcribe_time datetime comment 'è®¢éæ¶é´',
    send_time     datetime comment 'åéæ¶é´',
    notice_type   tinyint(1) comment 'éç¥æ¹å¼[0-ç­ä¿¡ï¼1-é®ä»¶]',
    primary key (id)
);

alter table sms_seckill_sku_notice
    comment 'ç§æååéç¥è®¢é';

/*==============================================================*/
/* Table: sms_seckill_sku_relation                              */
/*==============================================================*/
create table sms_seckill_sku_relation
(
    id                   bigint not null auto_increment comment 'id',
    promotion_id         bigint comment 'æ´»å¨id',
    promotion_session_id bigint comment 'æ´»å¨åºæ¬¡id',
    sku_id               bigint comment 'ååid',
    seckill_price        decimal comment 'ç§æä»·æ ¼',
    seckill_count        decimal comment 'ç§ææ»é',
    seckill_limit        decimal comment 'æ¯äººéè´­æ°é',
    seckill_sort         int comment 'æåº',
    primary key (id)
);

alter table sms_seckill_sku_relation
    comment 'ç§ææ´»å¨ååå³è';

/*==============================================================*/
/* Table: sms_sku_full_reduction                                */
/*==============================================================*/
create table sms_sku_full_reduction
(
    id           bigint not null auto_increment comment 'id',
    sku_id       bigint comment 'spu_id',
    full_price   decimal(18, 4) comment 'æ»¡å¤å°',
    reduce_price decimal(18, 4) comment 'åå¤å°',
    add_other    tinyint(1) comment 'æ¯å¦åä¸å¶ä»ä¼æ ',
    primary key (id)
);

alter table sms_sku_full_reduction
    comment 'ååæ»¡åä¿¡æ¯';

/*==============================================================*/
/* Table: sms_sku_ladder                                        */
/*==============================================================*/
create table sms_sku_ladder
(
    id         bigint not null auto_increment comment 'id',
    sku_id     bigint comment 'spu_id',
    full_count int comment 'æ»¡å ä»¶',
    discount   decimal(4, 2) comment 'æå æ',
    price      decimal(18, 4) comment 'æåä»·',
    add_other  tinyint(1) comment 'æ¯å¦å å å¶ä»ä¼æ [0-ä¸å¯å å ï¼1-å¯å å ]',
    primary key (id)
);

alter table sms_sku_ladder
    comment 'ååé¶æ¢¯ä»·æ ¼';

/*==============================================================*/
/* Table: sms_spu_bounds                                        */
/*==============================================================*/
create table sms_spu_bounds
(
    id          bigint not null auto_increment comment 'id',
    spu_id      bigint,
    grow_bounds decimal(18, 4) comment 'æé¿ç§¯å',
    buy_bounds  decimal(18, 4) comment 'è´­ç©ç§¯å',
    work        tinyint(1) comment 'ä¼æ çææåµ[1111ï¼åä¸ªç¶æä½ï¼ä»å³å°å·¦ï¼;0 - æ ä¼æ ï¼æé¿ç§¯åæ¯å¦èµ é;1 - æ ä¼æ ï¼è´­ç©ç§¯åæ¯å¦èµ é;2 - æä¼æ ï¼æé¿ç§¯åæ¯å¦èµ é;3 - æä¼æ ï¼è´­ç©ç§¯åæ¯å¦èµ éãç¶æä½0ï¼ä¸èµ éï¼1ï¼èµ éã]',
    primary key (id)
);

alter table sms_spu_bounds
    comment 'ååspuç§¯åè®¾ç½®';