drop table if exists ums_growth_change_history;

drop table if exists ums_integration_change_history;

drop table if exists ums_member;

drop table if exists ums_member_collect_spu;

drop table if exists ums_member_collect_subject;

drop table if exists ums_member_level;

drop table if exists ums_member_login_log;

drop table if exists ums_member_receive_address;

drop table if exists ums_member_statistics_info;

/*==============================================================*/
/* Table: ums_growth_change_history                             */
/*==============================================================*/
create table ums_growth_change_history
(
    id           bigint not null auto_increment comment 'id',
    member_id    bigint comment 'member_id',
    create_time  datetime comment 'create_time',
    change_count int comment 'æ¹åçå¼ï¼æ­£è´è®¡æ°ï¼',
    note         varchar(0) comment 'å¤æ³¨',
    source_type  tinyint comment 'ç§¯åæ¥æº[0-è´­ç©ï¼1-ç®¡çåä¿®æ¹]',
    primary key (id)
);

alter table ums_growth_change_history
    comment 'æé¿å¼åååå²è®°å½';

/*==============================================================*/
/* Table: ums_integration_change_history                        */
/*==============================================================*/
create table ums_integration_change_history
(
    id           bigint not null auto_increment comment 'id',
    member_id    bigint comment 'member_id',
    create_time  datetime comment 'create_time',
    change_count int comment 'ååçå¼',
    note         varchar(255) comment 'å¤æ³¨',
    source_tyoe  tinyint comment 'æ¥æº[0->è´­ç©ï¼1->ç®¡çåä¿®æ¹;2->æ´»å¨]',
    primary key (id)
);

alter table ums_integration_change_history
    comment 'ç§¯ååååå²è®°å½';

/*==============================================================*/
/* Table: ums_member                                            */
/*==============================================================*/
create table ums_member
(
    id          bigint not null auto_increment comment 'id',
    level_id    bigint comment 'ä¼åç­çº§id',
    username    char(64) comment 'ç¨æ·å',
    password    varchar(64) comment 'å¯ç ',
    nickname    varchar(64) comment 'æµç§°',
    mobile      varchar(20) comment 'ææºå·ç ',
    email       varchar(64) comment 'é®ç®±',
    header      varchar(500) comment 'å¤´å',
    gender      tinyint comment 'æ§å«',
    birth       date comment 'çæ¥',
    city        varchar(500) comment 'æå¨åå¸',
    job         varchar(255) comment 'èä¸',
    sign        varchar(255) comment 'ä¸ªæ§ç­¾å',
    source_type tinyint comment 'ç¨æ·æ¥æº',
    integration int comment 'ç§¯å',
    growth      int comment 'æé¿å¼',
    status      tinyint comment 'å¯ç¨ç¶æ',
    create_time datetime comment 'æ³¨åæ¶é´',
    primary key (id)
);

alter table ums_member
    comment 'ä¼å';

/*==============================================================*/
/* Table: ums_member_collect_spu                                */
/*==============================================================*/
create table ums_member_collect_spu
(
    id          bigint not null comment 'id',
    member_id   bigint comment 'ä¼åid',
    spu_id      bigint comment 'spu_id',
    spu_name    varchar(500) comment 'spu_name',
    spu_img     varchar(500) comment 'spu_img',
    create_time datetime comment 'create_time',
    primary key (id)
);

alter table ums_member_collect_spu
    comment 'ä¼åæ¶èçåå';

/*==============================================================*/
/* Table: ums_member_collect_subject                            */
/*==============================================================*/
create table ums_member_collect_subject
(
    id           bigint not null auto_increment comment 'id',
    subject_id   bigint comment 'subject_id',
    subject_name varchar(255) comment 'subject_name',
    subject_img  varchar(500) comment 'subject_img',
    subject_urll varchar(500) comment 'æ´»å¨url',
    primary key (id)
);

alter table ums_member_collect_subject
    comment 'ä¼åæ¶èçä¸é¢æ´»å¨';

/*==============================================================*/
/* Table: ums_member_level                                      */
/*==============================================================*/
create table ums_member_level
(
    id                      bigint not null auto_increment comment 'id',
    name                    varchar(100) comment 'ç­çº§åç§°',
    growth_point            int comment 'ç­çº§éè¦çæé¿å¼',
    default_status          tinyint comment 'æ¯å¦ä¸ºé»è®¤ç­çº§[0->ä¸æ¯ï¼1->æ¯]',
    free_freight_point      decimal(18, 4) comment 'åè¿è´¹æ å',
    comment_growth_point    int comment 'æ¯æ¬¡è¯ä»·è·åçæé¿å¼',
    priviledge_free_freight tinyint comment 'æ¯å¦æåé®ç¹æ',
    priviledge_member_price tinyint comment 'æ¯å¦æä¼åä»·æ ¼ç¹æ',
    priviledge_birthday     tinyint comment 'æ¯å¦æçæ¥ç¹æ',
    note                    varchar(255) comment 'å¤æ³¨',
    primary key (id)
);

alter table ums_member_level
    comment 'ä¼åç­çº§';

/*==============================================================*/
/* Table: ums_member_login_log                                  */
/*==============================================================*/
create table ums_member_login_log
(
    id          bigint not null auto_increment comment 'id',
    member_id   bigint comment 'member_id',
    create_time datetime comment 'åå»ºæ¶é´',
    ip          varchar(64) comment 'ip',
    city        varchar(64) comment 'city',
    login_type  tinyint(1) comment 'ç»å½ç±»å[1-webï¼2-app]',
    primary key (id)
);

alter table ums_member_login_log
    comment 'ä¼åç»å½è®°å½';

/*==============================================================*/
/* Table: ums_member_receive_address                            */
/*==============================================================*/
create table ums_member_receive_address
(
    id             bigint not null auto_increment comment 'id',
    member_id      bigint comment 'member_id',
    name           varchar(255) comment 'æ¶è´§äººå§å',
    phone          varchar(64) comment 'çµè¯',
    post_code      varchar(64) comment 'é®æ¿ç¼ç ',
    province       varchar(100) comment 'çä»½/ç´è¾å¸',
    city           varchar(100) comment 'åå¸',
    region         varchar(100) comment 'åº',
    detail_address varchar(255) comment 'è¯¦ç»å°å(è¡é)',
    areacode       varchar(15) comment 'çå¸åºä»£ç ',
    default_status tinyint(1) comment 'æ¯å¦é»è®¤',
    primary key (id)
);

alter table ums_member_receive_address
    comment 'ä¼åæ¶è´§å°å';

/*==============================================================*/
/* Table: ums_member_statistics_info                            */
/*==============================================================*/
create table ums_member_statistics_info
(
    id                    bigint not null auto_increment comment 'id',
    member_id             bigint comment 'ä¼åid',
    consume_amount        decimal(18, 4) comment 'ç´¯è®¡æ¶è´¹éé¢',
    coupon_amount         decimal(18, 4) comment 'ç´¯è®¡ä¼æ éé¢',
    order_count           int comment 'è®¢åæ°é',
    coupon_count          int comment 'ä¼æ å¸æ°é',
    comment_count         int comment 'è¯ä»·æ°',
    return_order_count    int comment 'éè´§æ°é',
    login_count           int comment 'ç»å½æ¬¡æ°',
    attend_count          int comment 'å³æ³¨æ°é',
    fans_count            int comment 'ç²ä¸æ°é',
    collect_product_count int comment 'æ¶èçååæ°é',
    collect_subject_count int comment 'æ¶èçä¸é¢æ´»å¨æ°é',
    collect_comment_count int comment 'æ¶èçè¯è®ºæ°é',
    invite_friend_count   int comment 'éè¯·çæåæ°é',
    primary key (id)
);

alter table ums_member_statistics_info
    comment 'ä¼åç»è®¡ä¿¡æ¯';