/*
SQLyog Ultimate v11.25 (64 bit)
MySQL - 5.7.27 : Database - gulimall_wms
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE = ''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS = @@UNIQUE_CHECKS, UNIQUE_CHECKS = 0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS = 0 */;
/*!40101 SET @OLD_SQL_MODE = @@SQL_MODE, SQL_MODE = 'NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES = @@SQL_NOTES, SQL_NOTES = 0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS */`gulimall_wms` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `gulimall_wms`;

/*Table structure for table `undo_log` */

DROP TABLE IF EXISTS `undo_log`;

CREATE TABLE `undo_log`
(
    `id`            bigint(20)   NOT NULL AUTO_INCREMENT,
    `branch_id`     bigint(20)   NOT NULL,
    `xid`           varchar(100) NOT NULL,
    `context`       varchar(128) NOT NULL,
    `rollback_info` longblob     NOT NULL,
    `log_status`    int(11)      NOT NULL,
    `log_created`   datetime     NOT NULL,
    `log_modified`  datetime     NOT NULL,
    `ext`           varchar(100) DEFAULT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `ux_undo_log` (`xid`, `branch_id`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

/*Data for the table `undo_log` */

/*Table structure for table `wms_purchase` */

DROP TABLE IF EXISTS `wms_purchase`;

CREATE TABLE `wms_purchase`
(
    `id`            bigint(20) NOT NULL AUTO_INCREMENT,
    `assignee_id`   bigint(20)     DEFAULT NULL,
    `assignee_name` varchar(255)   DEFAULT NULL,
    `phone`         char(13)       DEFAULT NULL,
    `priority`      int(4)         DEFAULT NULL,
    `status`        int(4)         DEFAULT NULL,
    `ware_id`       bigint(20)     DEFAULT NULL,
    `amount`        decimal(18, 4) DEFAULT NULL,
    `create_time`   datetime       DEFAULT NULL,
    `update_time`   datetime       DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4 COMMENT ='éè´­ä¿¡æ¯';

/*Data for the table `wms_purchase` */

/*Table structure for table `wms_purchase_detail` */

DROP TABLE IF EXISTS `wms_purchase_detail`;

CREATE TABLE `wms_purchase_detail`
(
    `id`          bigint(20) NOT NULL AUTO_INCREMENT,
    `purchase_id` bigint(20)     DEFAULT NULL COMMENT 'éè´­åid',
    `sku_id`      bigint(20)     DEFAULT NULL COMMENT 'éè´­ååid',
    `sku_num`     int(11)        DEFAULT NULL COMMENT 'éè´­æ°é',
    `sku_price`   decimal(18, 4) DEFAULT NULL COMMENT 'éè´­éé¢',
    `ware_id`     bigint(20)     DEFAULT NULL COMMENT 'ä»åºid',
    `status`      int(11)        DEFAULT NULL COMMENT 'ç¶æ[0æ°å»ºï¼1å·²åéï¼2æ­£å¨éè´­ï¼3å·²å®æï¼4éè´­å¤±è´¥]',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4;

/*Data for the table `wms_purchase_detail` */

/*Table structure for table `wms_ware_info` */

DROP TABLE IF EXISTS `wms_ware_info`;

CREATE TABLE `wms_ware_info`
(
    `id`       bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
    `name`     varchar(255) DEFAULT NULL COMMENT 'ä»åºå',
    `address`  varchar(255) DEFAULT NULL COMMENT 'ä»åºå°å',
    `areacode` varchar(20)  DEFAULT NULL COMMENT 'åºåç¼ç ',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4 COMMENT ='ä»åºä¿¡æ¯';

/*Data for the table `wms_ware_info` */

/*Table structure for table `wms_ware_order_task` */

DROP TABLE IF EXISTS `wms_ware_order_task`;

CREATE TABLE `wms_ware_order_task`
(
    `id`               bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
    `order_id`         bigint(20)   DEFAULT NULL COMMENT 'order_id',
    `order_sn`         varchar(255) DEFAULT NULL COMMENT 'order_sn',
    `consignee`        varchar(100) DEFAULT NULL COMMENT 'æ¶è´§äºº',
    `consignee_tel`    char(15)     DEFAULT NULL COMMENT 'æ¶è´§äººçµè¯',
    `delivery_address` varchar(500) DEFAULT NULL COMMENT 'ééå°å',
    `order_comment`    varchar(200) DEFAULT NULL COMMENT 'è®¢åå¤æ³¨',
    `payment_way`      tinyint(1)   DEFAULT NULL COMMENT 'ä»æ¬¾æ¹å¼ã 1:å¨çº¿ä»æ¬¾ 2:è´§å°ä»æ¬¾ã',
    `task_status`      tinyint(2)   DEFAULT NULL COMMENT 'ä»»å¡ç¶æ',
    `order_body`       varchar(255) DEFAULT NULL COMMENT 'è®¢åæè¿°',
    `tracking_no`      char(30)     DEFAULT NULL COMMENT 'ç©æµåå·',
    `create_time`      datetime     DEFAULT NULL COMMENT 'create_time',
    `ware_id`          bigint(20)   DEFAULT NULL COMMENT 'ä»åºid',
    `task_comment`     varchar(500) DEFAULT NULL COMMENT 'å·¥ä½åå¤æ³¨',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4 COMMENT ='åºå­å·¥ä½å';

/*Data for the table `wms_ware_order_task` */

/*Table structure for table `wms_ware_order_task_detail` */

DROP TABLE IF EXISTS `wms_ware_order_task_detail`;

CREATE TABLE `wms_ware_order_task_detail`
(
    `id`          bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
    `sku_id`      bigint(20)   DEFAULT NULL COMMENT 'sku_id',
    `sku_name`    varchar(255) DEFAULT NULL COMMENT 'sku_name',
    `sku_num`     int(11)      DEFAULT NULL COMMENT 'è´­ä¹°ä¸ªæ°',
    `task_id`     bigint(20)   DEFAULT NULL COMMENT 'å·¥ä½åid',
    `ware_id`     bigint(20)   DEFAULT NULL COMMENT 'ä»åºid',
    `lock_status` int(1)       DEFAULT NULL COMMENT '1-å·²éå®  2-å·²è§£é  3-æ£å',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4 COMMENT ='åºå­å·¥ä½å';

/*Data for the table `wms_ware_order_task_detail` */

/*Table structure for table `wms_ware_sku` */

DROP TABLE IF EXISTS `wms_ware_sku`;

CREATE TABLE `wms_ware_sku`
(
    `id`           bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
    `sku_id`       bigint(20)   DEFAULT NULL COMMENT 'sku_id',
    `ware_id`      bigint(20)   DEFAULT NULL COMMENT 'ä»åºid',
    `stock`        int(11)      DEFAULT NULL COMMENT 'åºå­æ°',
    `sku_name`     varchar(200) DEFAULT NULL COMMENT 'sku_name',
    `stock_locked` int(11)      DEFAULT '0' COMMENT 'éå®åºå­',
    PRIMARY KEY (`id`),
    KEY `sku_id` (`sku_id`) USING BTREE,
    KEY `ware_id` (`ware_id`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4 COMMENT ='åååºå­';

/*Data for the table `wms_ware_sku` */

/*!40101 SET SQL_MODE = @OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS = @OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES = @OLD_SQL_NOTES */;