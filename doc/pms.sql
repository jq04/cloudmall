drop table if exists pms_attr;

drop table if exists pms_attr_attrgroup_relation;

drop table if exists pms_attr_group;

drop table if exists pms_brand;

drop table if exists pms_category;

drop table if exists pms_category_brand_relation;

drop table if exists pms_comment_replay;

drop table if exists pms_product_attr_value;

drop table if exists pms_sku_images;

drop table if exists pms_sku_info;

drop table if exists pms_sku_sale_attr_value;

drop table if exists pms_spu_comment;

drop table if exists pms_spu_images;

drop table if exists pms_spu_info;

drop table if exists pms_spu_info_desc;

/*==============================================================*/
/* Table: pms_attr                                              */
/*==============================================================*/
create table pms_attr
(
    attr_id      bigint not null auto_increment comment 'å±æ§id',
    attr_name    char(30) comment 'å±æ§å',
    search_type  tinyint comment 'æ¯å¦éè¦æ£ç´¢[0-ä¸éè¦ï¼1-éè¦]',
    icon         varchar(255) comment 'å±æ§å¾æ ',
    value_select char(255) comment 'å¯éå¼åè¡¨[ç¨éå·åé]',
    attr_type    tinyint comment 'å±æ§ç±»å[0-éå®å±æ§ï¼1-åºæ¬å±æ§ï¼2-æ¢æ¯éå®å±æ§åæ¯åºæ¬å±æ§]',
    enable       bigint comment 'å¯ç¨ç¶æ[0 - ç¦ç¨ï¼1 - å¯ç¨]',
    catelog_id   bigint comment 'æå±åç±»',
    show_desc    tinyint comment 'å¿«éå±ç¤ºãæ¯å¦å±ç¤ºå¨ä»ç»ä¸ï¼0-å¦ 1-æ¯ãï¼å¨skuä¸­ä»ç¶å¯ä»¥è°æ´',
    primary key (attr_id)
);

alter table pms_attr
    comment 'ååå±æ§';

/*==============================================================*/
/* Table: pms_attr_attrgroup_relation                           */
/*==============================================================*/
create table pms_attr_attrgroup_relation
(
    id            bigint not null auto_increment comment 'id',
    attr_id       bigint comment 'å±æ§id',
    attr_group_id bigint comment 'å±æ§åç»id',
    attr_sort     int comment 'å±æ§ç»åæåº',
    primary key (id)
);

alter table pms_attr_attrgroup_relation
    comment 'å±æ§&å±æ§åç»å³è';

/*==============================================================*/
/* Table: pms_attr_group                                        */
/*==============================================================*/
create table pms_attr_group
(
    attr_group_id   bigint not null auto_increment comment 'åç»id',
    attr_group_name char(20) comment 'ç»å',
    sort            int comment 'æåº',
    descript        varchar(255) comment 'æè¿°',
    icon            varchar(255) comment 'ç»å¾æ ',
    catelog_id      bigint comment 'æå±åç±»id',
    primary key (attr_group_id)
);

alter table pms_attr_group
    comment 'å±æ§åç»';

/*==============================================================*/
/* Table: pms_brand                                             */
/*==============================================================*/
create table pms_brand
(
    brand_id     bigint not null auto_increment comment 'åçid',
    name         char(50) comment 'åçå',
    logo         varchar(2000) comment 'åçlogoå°å',
    descript     longtext comment 'ä»ç»',
    show_status  tinyint comment 'æ¾ç¤ºç¶æ[0-ä¸æ¾ç¤ºï¼1-æ¾ç¤º]',
    first_letter char(1) comment 'æ£ç´¢é¦å­æ¯',
    sort         int comment 'æåº',
    primary key (brand_id)
);

alter table pms_brand
    comment 'åç';

/*==============================================================*/
/* Table: pms_category                                          */
/*==============================================================*/
create table pms_category
(
    cat_id        bigint not null auto_increment comment 'åç±»id',
    name          char(50) comment 'åç±»åç§°',
    parent_cid    bigint comment 'ç¶åç±»id',
    cat_level     int comment 'å±çº§',
    show_status   tinyint comment 'æ¯å¦æ¾ç¤º[0-ä¸æ¾ç¤ºï¼1æ¾ç¤º]',
    sort          int comment 'æåº',
    icon          char(255) comment 'å¾æ å°å',
    product_unit  char(50) comment 'è®¡éåä½',
    product_count int comment 'ååæ°é',
    primary key (cat_id)
);

alter table pms_category
    comment 'ååä¸çº§åç±»';

/*==============================================================*/
/* Table: pms_category_brand_relation                           */
/*==============================================================*/
create table pms_category_brand_relation
(
    id           bigint not null auto_increment,
    brand_id     bigint comment 'åçid',
    catelog_id   bigint comment 'åç±»id',
    brand_name   varchar(255),
    catelog_name varchar(255),
    primary key (id)
);

alter table pms_category_brand_relation
    comment 'åçåç±»å³è';

/*==============================================================*/
/* Table: pms_comment_replay                                    */
/*==============================================================*/
create table pms_comment_replay
(
    id         bigint not null auto_increment comment 'id',
    comment_id bigint comment 'è¯è®ºid',
    reply_id   bigint comment 'åå¤id',
    primary key (id)
);

alter table pms_comment_replay
    comment 'ååè¯ä»·åå¤å³ç³»';

/*==============================================================*/
/* Table: pms_product_attr_value                                */
/*==============================================================*/
create table pms_product_attr_value
(
    id         bigint not null auto_increment comment 'id',
    spu_id     bigint comment 'ååid',
    attr_id    bigint comment 'å±æ§id',
    attr_name  varchar(200) comment 'å±æ§å',
    attr_value varchar(200) comment 'å±æ§å¼',
    attr_sort  int comment 'é¡ºåº',
    quick_show tinyint comment 'å¿«éå±ç¤ºãæ¯å¦å±ç¤ºå¨ä»ç»ä¸ï¼0-å¦ 1-æ¯ã',
    primary key (id)
);

alter table pms_product_attr_value
    comment 'spuå±æ§å¼';

/*==============================================================*/
/* Table: pms_sku_images                                        */
/*==============================================================*/
create table pms_sku_images
(
    id          bigint not null auto_increment comment 'id',
    sku_id      bigint comment 'sku_id',
    img_url     varchar(255) comment 'å¾çå°å',
    img_sort    int comment 'æåº',
    default_img int comment 'é»è®¤å¾[0 - ä¸æ¯é»è®¤å¾ï¼1 - æ¯é»è®¤å¾]',
    primary key (id)
);

alter table pms_sku_images
    comment 'skuå¾ç';

/*==============================================================*/
/* Table: pms_sku_info                                          */
/*==============================================================*/
create table pms_sku_info
(
    sku_id          bigint not null auto_increment comment 'skuId',
    spu_id          bigint comment 'spuId',
    sku_name        varchar(255) comment 'skuåç§°',
    sku_desc        varchar(2000) comment 'skuä»ç»æè¿°',
    catalog_id      bigint comment 'æå±åç±»id',
    brand_id        bigint comment 'åçid',
    sku_default_img varchar(255) comment 'é»è®¤å¾ç',
    sku_title       varchar(255) comment 'æ é¢',
    sku_subtitle    varchar(2000) comment 'å¯æ é¢',
    price           decimal(18, 4) comment 'ä»·æ ¼',
    sale_count      bigint comment 'éé',
    primary key (sku_id)
);

alter table pms_sku_info
    comment 'skuä¿¡æ¯';

/*==============================================================*/
/* Table: pms_sku_sale_attr_value                               */
/*==============================================================*/
create table pms_sku_sale_attr_value
(
    id         bigint not null auto_increment comment 'id',
    sku_id     bigint comment 'sku_id',
    attr_id    bigint comment 'attr_id',
    attr_name  varchar(200) comment 'éå®å±æ§å',
    attr_value varchar(200) comment 'éå®å±æ§å¼',
    attr_sort  int comment 'é¡ºåº',
    primary key (id)
);

alter table pms_sku_sale_attr_value
    comment 'skuéå®å±æ§&å¼';

/*==============================================================*/
/* Table: pms_spu_comment                                       */
/*==============================================================*/
create table pms_spu_comment
(
    id               bigint not null auto_increment comment 'id',
    sku_id           bigint comment 'sku_id',
    spu_id           bigint comment 'spu_id',
    spu_name         varchar(255) comment 'åååå­',
    member_nick_name varchar(255) comment 'ä¼åæµç§°',
    star             tinyint(1) comment 'æçº§',
    member_ip        varchar(64) comment 'ä¼åip',
    create_time      datetime comment 'åå»ºæ¶é´',
    show_status      tinyint(1) comment 'æ¾ç¤ºç¶æ[0-ä¸æ¾ç¤ºï¼1-æ¾ç¤º]',
    spu_attributes   varchar(255) comment 'è´­ä¹°æ¶å±æ§ç»å',
    likes_count      int comment 'ç¹èµæ°',
    reply_count      int comment 'åå¤æ°',
    resources        varchar(1000) comment 'è¯è®ºå¾ç/è§é¢[jsonæ°æ®ï¼[{type:æä»¶ç±»å,url:èµæºè·¯å¾}]]',
    content          text comment 'åå®¹',
    member_icon      varchar(255) comment 'ç¨æ·å¤´å',
    comment_type     tinyint comment 'è¯è®ºç±»å[0 - å¯¹ååçç´æ¥è¯è®ºï¼1 - å¯¹è¯è®ºçåå¤]',
    primary key (id)
);

alter table pms_spu_comment
    comment 'ååè¯ä»·';

/*==============================================================*/
/* Table: pms_spu_images                                        */
/*==============================================================*/
create table pms_spu_images
(
    id          bigint not null auto_increment comment 'id',
    spu_id      bigint comment 'spu_id',
    img_name    varchar(200) comment 'å¾çå',
    img_url     varchar(255) comment 'å¾çå°å',
    img_sort    int comment 'é¡ºåº',
    default_img tinyint comment 'æ¯å¦é»è®¤å¾',
    primary key (id)
);

alter table pms_spu_images
    comment 'spuå¾ç';

/*==============================================================*/
/* Table: pms_spu_info                                          */
/*==============================================================*/
create table pms_spu_info
(
    id              bigint not null auto_increment comment 'ååid',
    spu_name        varchar(200) comment 'åååç§°',
    spu_description varchar(1000) comment 'ååæè¿°',
    catalog_id      bigint comment 'æå±åç±»id',
    brand_id        bigint comment 'åçid',
    weight          decimal(18, 4),
    publish_status  tinyint comment 'ä¸æ¶ç¶æ[0 - ä¸æ¶ï¼1 - ä¸æ¶]',
    create_time     datetime,
    update_time     datetime,
    primary key (id)
);

alter table pms_spu_info
    comment 'spuä¿¡æ¯';

/*==============================================================*/
/* Table: pms_spu_info_desc                                     */
/*==============================================================*/
create table pms_spu_info_desc
(
    spu_id  bigint not null comment 'ååid',
    decript longtext comment 'ååä»ç»',
    primary key (spu_id)
);

alter table pms_spu_info_desc
    comment 'spuä¿¡æ¯ä»ç»';