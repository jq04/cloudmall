package com.touchsun.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.touchsun.common.utils.PageUtils;
import com.touchsun.product.entity.BrandEntity;

import java.util.Map;

/**
 * 品牌
 *
 * @author touchsun
 * @email mr.jiaqi1204@gmail.com
 * @date 2023-02-14 16:46:45
 */
public interface BrandService extends IService<BrandEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

