package com.touchsun.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.touchsun.common.utils.PageUtils;
import com.touchsun.product.entity.SpuInfoDescEntity;

import java.util.Map;

/**
 * spu信息介绍
 *
 * @author touchsun
 * @email mr.jiaqi1204@gmail.com
 * @date 2023-02-14 16:46:45
 */
public interface SpuInfoDescService extends IService<SpuInfoDescEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

