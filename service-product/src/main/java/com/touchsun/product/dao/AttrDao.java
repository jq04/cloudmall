package com.touchsun.product.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.touchsun.product.entity.AttrEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品属性
 *
 * @author touchsun
 * @email mr.jiaqi1204@gmail.com
 * @date 2023-02-14 16:46:45
 */
@Mapper
public interface AttrDao extends BaseMapper<AttrEntity> {

}
