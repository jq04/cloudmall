package com.touchsun.product;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.touchsun.product.entity.BrandEntity;
import com.touchsun.product.service.BrandService;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
class ProductApplicationTests {

    @Autowired
    BrandService brandService;

    @Test
    void brandAdd() {
        BrandEntity brandEntity = new BrandEntity();
        brandEntity.setName("一个测试品牌");
        brandService.save(brandEntity);
    }

    @Test
    void brandUpdate() {
        BrandEntity brandEntity = new BrandEntity();
        brandEntity.setBrandId(1L);
        brandEntity.setName("Lee测试品牌");
        brandService.updateById(brandEntity);
    }

    @Test
    void brandQuery() {
        brandService.list(
                new QueryWrapper<BrandEntity>().like("name", "品牌")
        ).forEach(entity -> System.out.println(entity.toString()));
    }

}
