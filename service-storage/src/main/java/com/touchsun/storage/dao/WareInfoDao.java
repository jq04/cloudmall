package com.touchsun.storage.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.touchsun.storage.entity.WareInfoEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 仓库信息
 *
 * @author touchsun
 * @email mr.jiaqi1204@gmail.com
 * @date 2023-02-15 11:07:59
 */
@Mapper
public interface WareInfoDao extends BaseMapper<WareInfoEntity> {

}
