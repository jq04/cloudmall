package com.touchsun.storage.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.touchsun.storage.entity.PurchaseDetailEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author touchsun
 * @email mr.jiaqi1204@gmail.com
 * @date 2023-02-15 11:07:59
 */
@Mapper
public interface PurchaseDetailDao extends BaseMapper<PurchaseDetailEntity> {

}
