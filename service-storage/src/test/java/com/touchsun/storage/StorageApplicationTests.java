package com.touchsun.storage;

import com.touchsun.storage.service.PurchaseService;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
class StorageApplicationTests {

    @Autowired
    PurchaseService purchaseService;

    @Test
    void contextLoads() {
        System.out.println(purchaseService.list());
    }

}
